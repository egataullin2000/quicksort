import java.util.LinkedList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        DataProvider dataProvider = DataProvider.get();
        int testsCount = 50 + new Random().nextInt(50);
        for (int i = 0; i < testsCount; i++) {
            int[] data = generateRandomData();
            //writes data two times because we'll need to read it two times (for array and for LinkedList)
            dataProvider.write(data);
            dataProvider.write(data);
        }
        dataProvider.closeWriter();
        for (int i = 0; i < testsCount; i++) {
            int[] dataArray = dataProvider.readArray();
            LinkedList<Integer> dataList = dataProvider.readList();
            long startTime = System.nanoTime();
            int iterationsCount = SortUtility.sort(dataArray);
            notifyArraySorted(dataArray.length, System.nanoTime() - startTime, iterationsCount);
            startTime = System.nanoTime();
            iterationsCount = SortUtility.sort(dataList);
            notifyListSorted(dataList.size(), System.nanoTime() - startTime, iterationsCount);
        }
        dataProvider.close();
    }

    private static int[] generateRandomData() {
        int[] data = new int[100 + new Random().nextInt(9_900)];
        for (int i = 0; i < data.length; i++) {
            data[i] = new Random().nextInt();
        }

        return data;
    }

    private static void notifyArraySorted(int length, long time, int iterationsCount) {
        System.out.println("Array with " + length + " elements was sorted in " + time + " ns (" + iterationsCount + " iterations)");
    }

    private static void notifyListSorted(int size, long time, int iterationsCount) {
        System.out.println("List with " + size + " elements was sorted in " + time + " ns (" + iterationsCount + " iterations)");
    }

}
