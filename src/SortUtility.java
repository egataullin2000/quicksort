import org.jetbrains.annotations.NotNull;

import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

public class SortUtility {

    private static volatile AtomicInteger iterationsCount;

    /**
     * Sorts array and returns count of iterations.
     */
    public static int sort(int[] array) {
        iterationsCount = new AtomicInteger();
        sort(array, 0, array.length - 1);
        return iterationsCount.intValue();
    }

    /**
     * Sorts list and returns count of iterations.
     */
    public static int sort(LinkedList<Integer> list) {
        iterationsCount = new AtomicInteger();
        sort(list, 0, list.size() - 1);
        return iterationsCount.intValue();
    }

    private static void sort(int[] array, int low, int high) {
        iterationsCount.incrementAndGet();
        if (array.length == 0 || low >= high) return;

        int middle = low + (high - low) / 2;
        int i = low, j = high;
        while (i <= j) {
            while (array[i] < array[middle]) {
                i++;
            }
            while (array[j] > array[middle]) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i++] = array[j];
                array[j--] = temp;
            }
        }

        if (low < j) sort(array, low, j);
        if (high > i) sort(array, i, high);
    }

    private static void sort(LinkedList<Integer> list, int low, int high) {
        iterationsCount.incrementAndGet();
        if (list.isEmpty() || low >= high) return;

        int middle = low + (high - low) / 2;
        int i = low, j = high;
        while (i <= j) {
            while (list.get(i) < list.get(middle)) {
                i++;
            }
            while (list.get(j) > list.get(middle)) {
                j--;
            }
            if (i <= j) {
                int temp = list.get(i);
                list.set(i++, list.get(j));
                list.set(j--, temp);
            }
        }

        if (low < j) new BackgroundListSorter(list, low, j).execute();
        if (high > i) sort(list, i, high);
    }

    private static class BackgroundListSorter implements Executor {
        private LinkedList<Integer> list;
        private int low, high;

        private BackgroundListSorter(LinkedList<Integer> list, int low, int high) {
            this.list = list;
            this.low = low;
            this.high = high;
        }

        private void execute() {
            execute(() -> sort(list, low, high));
        }

        @Override
        public void execute(@NotNull Runnable runnable) {
            new Thread(runnable).start();
        }
    }

}
