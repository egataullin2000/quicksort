import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class DataProvider {

    private static final String SOURCE = "data.txt";

    private static DataProvider instance;

    private FileWriter writer;
    private Scanner scanner;

    private DataProvider() {
        try {
            writer = new FileWriter(SOURCE);
            scanner = new Scanner(new FileReader(SOURCE));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DataProvider get() {
        if (instance == null) instance = new DataProvider();

        return instance;
    }

    public void write(int[] data) {
        try {
            writer.write(data.length + "\n");
            for (int value : data) {
                writer.write(value + " ");
            }
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int[] readArray() {
        int[] data = new int[scanner.nextInt()];
        for (int i = 0; i < data.length; i++) {
            data[i] = scanner.nextInt();
        }

        return data;
    }

    public LinkedList<Integer> readList() {
        LinkedList<Integer> data = new LinkedList<>();
        int itemsCount = scanner.nextInt();
        for (int i = 0; i < itemsCount; i++) {
            data.add(scanner.nextInt());
        }

        return data;
    }

    public void closeWriter() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            writer.close();
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
